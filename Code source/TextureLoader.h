// Class TextureLoader
// Auteur: Lo�c Chamberlin
// Objectif: G�rer de mani�re simplifi�e l'affiche d'images � l'�cran
//

#pragma once
#include <SFML/Graphics.hpp>
#include <sfeMovie/Movie.hpp>
#include <sfeMovie/Visibility.hpp>
#include <sfeMovie/StreamSelection.hpp>
#include <iostream>
#include <SFML\Audio\Music.hpp>
#include <string>

using namespace std;

class TextureLoader {
private:
	//Dimensions de l'objet
	float xscale_;
	float yscale_;

	//Position de l'objet
	int xpos_;
	int ypos_;

	//Nom du fichier
	string nom_;
	//Sprite � afficher
	sf::Sprite sprite_;
	//Texture utilis�e pour g�n�rer le Sprite
	sf::Texture texture_;

	//R�f�rence vers la fen�tre o� afficher le Sprite
	sf::RenderWindow* window_;

	//Si l'objet est anim�
	int fadeDelay_;
	int fadeDuration_;
	int fadeState_;

public:
	//Constructeur
	//Prends plusieurs param�tres en compte
	//sf::RenderWindow& window                 Pointeur vers la fen�tre pour g�rer l'affichage
	//string nomTexture = ""                   Nom du fichier de la texture
	//int xpos = 0                             Position en x de la texture
	//int ypos = 0                             Position en y de la texture
	//float xscale = 1.0                       Grossissement de la texture en x
	//float yscale = 1.0                       Grossissement de la texture en y
	//float fadeduration = 0                   Dur�e de la disparition
	//float fadedelay = 0                      D�lai avant la disparition
	TextureLoader(sf::RenderWindow & window, string nomTexture = "", int xpos = 0, int ypos = 0, float xscale = 1.0, float yscale = 1.0, float fadeduration = 0, float fadedelay = 0);
	
	//Permet de changer la taille de la texture
	void setScale(float xscale, float yscale);

	//Permet de changer la position de la texture
	void setPosition(int xpos, int ypos);

	//Permet d'actualiser l'affichage de la texture
	void draw();
};