// Class GameLogic
// Auteur: Pierre TASSAN
// Objectif: G�rer la logique derri�re le menu de jeu
//
#pragma once
#include <SFML/Graphics.hpp>
#include <sfeMovie/Movie.hpp>
#include <sfeMovie/Visibility.hpp>
#include <sfeMovie/StreamSelection.hpp>
#include <iostream>
#include <SFML\Audio\Music.hpp>
#include <string>
#include "WordsListener.h"
#include "GameHUD.h"
#include "EndHUD.h"

class GameLogic {
private:
	//R�f�rence � la fen�tre pour pouvoir g�rer l'affichage
	sf::RenderWindow* window_;

	//WordListener permettant de g�rer la saisie des mots � l'�cran
	WordsListener* listener_;
	
	//HUD du jeu et �galement de l'�cran de mort
	GameHUD* hud_;
	EndHUD* hudend_;

	//Permet de calculer combien de temps la partie a dur�
	sf::Clock timer_;

	//Stats du joueur
	int vie_;
	int score_;
	int combo_;
	int highestCombo_;
	int nbBonCaractere_;
	int nbMauvaisCaractere_;
	int nbWord_;
	int timeInSecond_;
	bool isPlaying_;
	bool isDead_;

	//Info sur le mode de jeu
	int gameMode_;
	
public:
	//Constructeur
	//Prend en param�tre la fen�tre pour ensuite la donner � son GameHUD (puis EndHUD)
	//Prend �galement en param�tre le mode de jeu
	//Gamemode 0 = normal 1= difficile 2=chaos
	GameLogic(sf::RenderWindow& window, int gameMode = 0);

	//Destructeur
	~GameLogic();

	//Enleve un point de vie au joueur
	void removeHealth();

	//Ajoute un point de vie au joueur
	void addHealth() { if (vie_ != 3) vie_ = vie_ + 1; };

	//Ajoute un nombre de points au joueur
	void addScore(int score);

	//Enleve un nombre de points au joueur
	//Pas utilis� mais peut �tre utile pour les futures mises � jour
	void removeScore(int score) { score_ = score_ - score; };

	//Permet d'ajouter 1 au combo actuel
	void addCombo();

	//Annule le combo actuel du joueur
	void resetCombo() { combo_ = 0; };
	
	//Met a jour les informations du joueur et indique � GameHUD de mettre � jour l'affichage
	void update();

	//Appel� lorsqu'une touche est appuy�e
	void typedKey(char key);

	//Permet de g�rer les clicks de souris
	void buttonClicked(float x, float y);

	//Indique si la partie est toujours en cours
	bool isPlaying() const { return isPlaying_; };

	//D�clenche la fin de la partie
	void endGame();

	//Ajoute un mot � la liste des mots affich�s
	void addWord();
};