﻿#include <SFML/Graphics.hpp>
#include <sfeMovie/Movie.hpp>
#include <sfeMovie/Visibility.hpp>
#include <sfeMovie/StreamSelection.hpp>
#include <iostream>
#include <SFML\Audio\Music.hpp>

#include "MenuCore.h"

using namespace std;


int WinMain()
{
	//Création de la fenêtre de jeu
	sf::RenderWindow window(sf::VideoMode(1920, 1080), "DactyloFall",
		sf::Style::Close | sf::Style::Fullscreen);

	//On crée un objet de la classe MenuCore
	MenuCore menucore(window);
	bool booting = true;
	int reponse = 0;
	while (reponse != 5) { //Tant que MenuCore ne retourne pas 5 (Soit l'action de fermer le jeu)
		reponse = menucore.MenuAccueil(booting); //On récupère sa réponse tout en lui indiquant si il doit demander à AccueilLogic d'afficher le texte de démarrage
		booting = false;
		if (reponse != 5) { //Si le joueur clique sur un des modes de jeu
			menucore.MenuGame(reponse); //On lance une partie
		}
	}

	return 0;
}