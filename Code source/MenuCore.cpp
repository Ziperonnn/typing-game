#include "MenuCore.h"


MenuCore::MenuCore(sf::RenderWindow& fenetre) {
	window_ = &fenetre;

	//D�finition des param�tres de la fen�tre
	window_->setFramerateLimit(60);
	window_->setVerticalSyncEnabled(true);
}

void MenuCore::MenuGame(int gamemode) {
	//On lance la partie en donnant le mode de jeu
	GameLogic game(*window_, gamemode);

	//Tant que nous sommes dans la partie
	while (game.isPlaying())
	{
		//On �coute 2 events
		//Les cliques de souris
		//Les touches tap�es au clavier
		sf::Event ev;
		while (window_->pollEvent(ev))
		{
			if (ev.type == sf::Event::MouseButtonPressed) {
				game.buttonClicked(ev.mouseButton.x, ev.mouseButton.y); //On donne � GameLogic les coordonn�es du clique

			}
			if (ev.type == sf::Event::TextEntered)
			{
				game.typedKey(static_cast<char>(ev.text.unicode)); //On donne � GameLogic, qui donnera ensuite � son WordListener, la lettre saisie
			}
		}
		//On met a jour l'affichage du jeu
		game.update();
	}
}

int MenuCore::MenuAccueil(bool booting) {
	//On affiche le menu d'accueil
	AccueilLogic accueil(*window_);

	int action = 6;
	//Tant que l'action retourn� par notre clique de souris de la part de AccueilLogic est 6 (soit un clique sur aucun bouton), nous continuons d'affiche le menu
	while (action == 6)
	{
		sf::Event ev;
		while (window_->pollEvent(ev))
		{
			if (ev.type == sf::Event::MouseButtonPressed) {
				action = accueil.buttonClicked(ev.mouseButton.x, ev.mouseButton.y); //on donne � AccueilLogic les coordonn�es du clique et on recup�re l'action � effectuer
			}
		}
		//On met � jour l'affichage du menu
		accueil.update(booting);
	}
	
	return action; //On retourne l'action au main
}