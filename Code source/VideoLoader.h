// Class VideoLoader
// Auteur: Lo�c Chamberlin
// Objectif: G�rer de mani�re simplifi�e l'affiche de vid�os � l'�cran
//
#pragma once
#include <SFML/Graphics.hpp>
#include <sfeMovie/Movie.hpp>
#include <sfeMovie/Visibility.hpp>
#include <sfeMovie/StreamSelection.hpp>
#include <iostream>
#include <SFML\Audio\Music.hpp>
#include <string>

using namespace std;

class VideoLoader {
private:
	//Dimensions de l'objet
	float xscale_;
	float yscale_;

	//Position de l'objet
	int xpos_;
	int ypos_;

	//Objet de la biblioth�que externe sfeMovie
	sfe::Movie movie_;

	//R�f�rence vers la fen�tre o� afficher la vid�o
	sf::RenderWindow* window_;

	//D�termine si la vid�o doit �tre lue en boucle
	bool isRepeated_;
public:
	//Constructeur
	//Prends plusieurs param�tres en compte
	//sf::RenderWindow& window                Pointeur vers la fen�tre pour g�rer l'affichage
	//string nomVideo                         Nom du fichier de la vid�o
	//float xscale                            Taille en x de la vid�o
	//float yscale                            Taille en y de la vid�o
	//int xpos                                Position en x de la vid�o
	//int ypos                                Position en y de la vid�o
	//bool isRepeated = false                 Si la vid�o est lue en boucle
	VideoLoader(sf::RenderWindow& window, string nomVideo, float xscale, float yscale, int xpos, int ypos, bool isRepeated = false);
	
	//Permet de changer la taille de la vid�o
	void setScale(float xscale, float yscale);
	
	//Permet de changer la position de la vid�o
	void setPosition(int xpos, int ypos);
	
	//Permet d'actualiser l'affichage de la vid�o
	void draw();
};
