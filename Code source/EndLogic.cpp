#include "EndLogic.h"

EndLogic::EndLogic(sf::RenderWindow& window, int gamemode, int score, int highestCombo, int nbWord, int nbBonCaractere, int nbMauvaisCaractere, int timeInSecond) {
	gameMode_ = gameMode_;
	score_ = score;
	highestCombo_ = highestCombo;
	nbBonCaractere_ = nbBonCaractere;
	nbMauvaisCaractere_ = nbMauvaisCaractere;
	timeInSecond_ = timeInSecond;


	hud_ = new EndHUD(window);
}

EndLogic::~EndLogic() {
	delete hud_;
}

void EndLogic::refresh() {
	hud_->refresh(gamemode, score, highestCombo, nbWord, nbBonCaractere, nbMauvaisCaractere, timeInSecond);
}