#include "SoundLoader.h"


SoundLoader::SoundLoader(string nomSon, int volume, bool loop, float xpos, float ypos, float zpos, bool play) {
	//Ouvrir le fichier de la musique
	music_.openFromFile(nomSon);
	//Changer les param�tres
	music_.setPosition(xpos, ypos, zpos); //Spacialisation du son
	music_.setPitch(1);
	music_.setVolume(volume); //Volume du son
	music_.setLoop(loop); //Choisir si le son boucle ou non
	//Si on a choisi de lancer la musique d�s le d�but
	if (play) {
		music_.play();
	}
}