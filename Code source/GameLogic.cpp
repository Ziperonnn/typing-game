#include "GameLogic.h"

GameLogic::GameLogic(sf::RenderWindow& window, int gameMode) { 
	window_ = &window;
	
	//Initialisation de la fen�tre de jeu
	hud_ = new GameHUD(window);

	//D�finition des param�tres pas d�faut
	vie_ = 3;
	gameMode_ = gameMode;

	//Initialisation du WordListener
	listener_ = new WordsListener(*window_, gameMode_);
	
	//D�marrage du timer de la partie
	timer_.restart();


	isPlaying_ = true;
	isDead_ = false;
}


void GameLogic::update() {
	//Si le joueur est mort
	if (isDead_) {
		//On affiche l'�cran de mort
		hudend_->refresh(gameMode_, score_, highestCombo_, nbWord_, nbBonCaractere_, nbMauvaisCaractere_, timeInSecond_);
	}
	else {
		//Sinon on affiche l'�cran de jeu
		hud_->refresh(vie_, score_, combo_, highestCombo_, listener_->returnWords());

		//On demande au WordListener si un mot est arriv� en bas de l'�cran
		if (listener_->updateWords()) {
			removeHealth(); //On enl�ve un point de vue
			addWord(); //On rajoute un nouveau mot
		}

		//En fonction du mode de jeu
		switch (gameMode_) {
		case 0: //Normal
			if (listener_->nbWord() < 3) { //Si le nombre de mots � l'�cran est inf�rieur � 3
				addWord(); //On ajoute un mot
			}
			break;
		case 1: //Difficile
			if (listener_->nbWord() < 4) { //Si le nombre de mots � l'�cran est inf�rieur � 4
				addWord();
			}
			break;

		case 2: //CHAOS
			if (listener_->nbWord() < 5) { //Si le nombre de mots � l'�cran est inf�rieur � 5
				addWord();
			}
			break;
		}

		if (vie_ == 0) { //Si le joueur n'a plus de vie, on met fin � la partie
			endGame();
		}
	}
	
}

void GameLogic::typedKey(char key) {
	// Quand une lettre est saisie	
	int resultat = listener_->typedKey(key); //On donne la lettre � WordListener et on recup�re son retour

	sf::Vector2f lastWord_ = listener_->lastDestuctionLocation(); //On r�cup�re l'endroit du mot que le joueur est en train de saisir
	
	//En fonction de ce que WordListener a retourn�
	switch (resultat) {
	case 0: //Si la lettre n'est pas bonne
		resetCombo();  //On reset le combo
		nbMauvaisCaractere_++;  //On incr�mente de nombre de mauvaises lettres saisies
		break;
	case 1: //Si la lettre est bonne
		addCombo(); //On incr�mente le combo
		nbBonCaractere_++; //On incr�mente de nombre de bonnes lettres saisies
		break;
	case 2: //Si le mot a �t� d�truit
		addWord(); //On affiche un nouveau mot
		addScore(100); //On ajoute 100 points
		nbWord_++; //On incr�mente de nombre de mots d�truits

		//On demande � GameHUD d'affiche un commentaire et une texture
		hud_->setCommentaire(new ComplexText(*window_, "Bien vu!", 30, sf::Color::Red, "minecraft.ttf", listener_->lastDestuctionLocation(), sf::Color::White, 0, -20, false, false, true, true, 1, 1.6, 50, 10));
		hud_->setTexture(new TextureLoader(*window_, "correct.png", listener_->lastDestuctionLocation().x, listener_->lastDestuctionLocation().y, 0.4, 0.4, 50, 10));
		
		break;
	case 3: //Si le mot a �t� d�truit et qu'il s'agit d'un mot bonus
		addHealth(); //On ajoute de la vie
		addWord(); //On affiche un nouveau mot
		addScore(500); //On ajoute 500 points
		nbWord_++; //On incr�mente de nombre de mots d�truits

		//On demande � GameHUD d'affiche un commentaire et une texture
		hud_->setCommentaire(new ComplexText(*window_, "Bonus +500!", 30, sf::Color::Yellow, "minecraft.ttf", listener_->lastDestuctionLocation(), sf::Color::White, 0, -20, false, false, true, true, 1, 1.6, 50, 10));
		hud_->setTexture(new TextureLoader(*window_, "correct.png", listener_->lastDestuctionLocation().x, listener_->lastDestuctionLocation().y, 0.4, 0.4, 50, 10));
		
		//on joue �galement un son de f�licitation
		hud_->getSound()->stop();
		hud_->getSound()->play();
		break;
	}
}

void GameLogic::addScore(int score) {
	score_ = score_ + score;
	//Si le score est un multiple de 1000, on donne une vie
	//On donne donc une vie au joueur tous les 1000 points et quand il complete un mot bonus
	if (score_ % 1000 == 0)
	{
		addHealth();
	}
}

void GameLogic::addWord() {

	float speed = 1;
	
	//On d�termine la vitesse du mot en fonction du mode de jeu
	switch (gameMode_) {
	case 0: //Normal 
		speed = 1; //Vitesse obligatoirement � 1
		break;
	case 1: //Difficile
		speed = randomIntMinMax(1, 2); //Vitesse entre 1 et 2
		break;

	case 2: //CHAOS
		speed = randomIntMinMax(1, 2);  //Vitesse entre 1 et 2
		//Mettre une vitesse � 3 rend le jeu impossible
		break;
	}

	//On demande au WordListener d'ajouter un mot avec la vitesse choisi
	listener_->addWord(speed);
}


void GameLogic::addCombo() {
	combo_++;
	//Si le combo est sup�rieur au meilleur combo, on met le meilleur combo � jour
	if (combo_ >= highestCombo_) {
		highestCombo_ = combo_;
	}
}


void GameLogic::removeHealth() {
	//On enl�ve une vie au joueur
	vie_--;
}


void GameLogic::buttonClicked(float x, float y) {
	//Le bouton quitter n'est pas le m�me si le joueur est en jeu ou sur le menu de gameover, on adapte donc
	if (isDead_) {
		if (hudend_->getQuitButton()->isClicked(x, y)) {
			isPlaying_ = false;
		}
	}
	else {
		if (hud_->getQuitButton()->isClicked(x, y)) {
			isPlaying_ = false;
		}
	}
	
}

GameLogic::~GameLogic() {
	//On d�truit le WordListener
	delete listener_;
	//Si le joueur est sur le menu de mort,  l'interface � d�truire est celle du menu GameOver
	if (isDead_) {
		delete hudend_;
	}
	else {
		//Sinon il s'agit de l'interface de jeu
		delete hud_;
	}

}

void GameLogic::endGame() {
	isDead_ = true;
	//Lorsque le joueur meurt, on supprime l'interface et on affiche l'�cran de mort
	delete hud_;
	timeInSecond_ = timer_.getElapsedTime().asSeconds(); //On calcule �galement le temps �coul� pendant la partie
	hudend_ = new EndHUD(*window_);
}