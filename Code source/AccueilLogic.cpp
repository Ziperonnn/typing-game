#define _CRT_SECURE_NO_DEPRECATE
#include "AccueilLogic.h"


AccueilLogic::AccueilLogic(sf::RenderWindow& window) {
	window_ = &window;

	//Initilisation l'interface graphique qui va ensuite g�rer de son c�t� l'affichage � l'�cran
	hud_ = new AccueilHUD(window);

	//Verifie si le fichier v.txt existe.
	//Si c'est le cas, alors le fond doit �tre une vid�o, sinon c'est une simple image
	FILE* file;
	if (file = fopen("v.txt", "r")) {
		fclose(file);
		video_ = true;

	}
	else {
		video_ = false;
	}
}

AccueilLogic::~AccueilLogic() {
	delete hud_; //On supprime la class qui g�re l'affichage graphique
}

void AccueilLogic::update(bool booting) {
	hud_->refresh(booting, video_); //Lorsque MenuCore indique � AccueilLogic d'actualiser, AccueilLogic va transmettre cette information � AccueilHUD qui va actualiser l'affichage graphique
}

int AccueilLogic::buttonClicked(float x, float y) {
	//Va retourner une valeur en fonction du bouton cliqu�.
	//Cette valeur sera ensuite interpr�t�e par MenuCore qui d�cidera de quoi faire
	if (hud_->getQuitButton()->isClicked(x, y)) {
		return 5;
	}
	if (hud_->getNormalButton()->isClicked(x, y)) {
		return 0;
	}
	if (hud_->getDifficileButton()->isClicked(x, y)) {
		return 1;
	}
	if (hud_->getChaosButton()->isClicked(x, y)) {
		return 2;
	}
	if (hud_->getVideoButton()->isClicked(x, y)) {
		//Si le bouton cliqu� est cela de l'affichage vid�o, alors c'est AccueilLogic qui va g�rer l'action � effectuer.
		//Si le fichier v.txt existe, alors il est supprim�, si il n'existe pas, il est cr��.
		//Ce fichier permet d'indiquer � GameHUD si une vid�o doit �tre affich�e en fond ou si il ne faut mettre qu'une image
		FILE* file;
		if (file = fopen("v.txt", "r")) {
			fclose(file);
			remove("v.txt");
			video_ = false;
		}
		else {
			ofstream fichier("v.txt");
			fichier.close();
			video_ = true;
		}
	}

	if (hud_->getSplashButton()->isClicked(x, y)) {
		FILE* file;
		if (file = fopen("w.txt", "r")) {
			fclose(file);
			remove("w.txt");
		}
		else {
			ofstream fichier("w.txt");
			fichier.close();
		}
		return 5;
	}

	return 6;
}