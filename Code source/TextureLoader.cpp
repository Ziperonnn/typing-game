#include "TextureLoader.h"

TextureLoader::TextureLoader(sf::RenderWindow& window, string nomTexture, int xpos, int ypos, float xscale, float yscale, float fadeduration, float fadedelay) {
	window_ = &window;
	nom_ = nomTexture;
	xscale_ = xscale;
	yscale_ = yscale;
	xpos_ = xpos;
	ypos_ = ypos;

	fadeDuration_ = fadeduration;
	fadeState_ = fadeduration;
	fadeDelay_ = fadedelay;

	texture_.loadFromFile(nom_);
	sprite_.setTexture(texture_);
	sprite_.setScale(xscale_, yscale_);
	sprite_.setPosition(xpos_, ypos_);
}

void TextureLoader::setScale(float xscale = 1.0, float yscale = 1.0) {
	sprite_.setScale(xscale, yscale);
}

void TextureLoader::setPosition(int xpos = 0, int ypos = 0) {
	sprite_.setPosition(xpos, ypos);
}

void TextureLoader::draw() {
	if (fadeDuration_ > 1) {
		if (fadeDelay_ > 0) {
			fadeDelay_--;
		}
		else {
			fadeDuration_--;
			int alpha = fadeDuration_ * 255 / fadeState_;
			sprite_.setColor(sf::Color(sprite_.getColor().r, sprite_.getColor().g, sprite_.getColor().b, alpha));
		}
	}
	else if (fadeDuration_ == 1) {
		sprite_.setColor(sf::Color(sprite_.getColor().r, sprite_.getColor().g, sprite_.getColor().b, 0));
	}

	window_->draw(sprite_);

}