#include "EndHUD.h"

EndHUD::EndHUD(sf::RenderWindow& window) {
	window_ = &window;

	//Initialisation de la vid�o en arri�re plan
	videoarriereplan_ = new VideoLoader(*window_, "arriereplanfin.mp4", window_->getSize().x, window_->getSize().y, 0, 0, true);
	
	//Initialisation de la musique de fond
	sonarriereplan_ = new SoundLoader("gameover.wav", 5, true, 0, 0, 0, true);

	//Initialisation du bouton
	quitter_ = new ComplexText(*window_, "Quitter", 30, sf::Color::Red, "neonavy.ttf", sf::Vector2f(1750, 1030), sf::Color::White, 2, 0);

	//Initialisation des diff�rents textes
	commentaire_ = new ComplexText(*window_, "Game over!", 60, sf::Color::Red, "minecraft.ttf", sf::Vector2f(0, 400), sf::Color::Black, 1, 0, true, false, false, true, 1.0, 1.05, 0, 0, 80, 50);
	score_ = new ComplexText(*window_, "Score", 25, sf::Color::White, "minecraft.ttf", sf::Vector2f(0, 500), sf::Color::White, 0, 0, true, false, false, false, 0, 0, 0, 0, 60, 140);
	keyboardstats_ = new ComplexText(*window_, "KeyBoard stats!", 25, sf::Color::White, "minecraft.ttf", sf::Vector2f(0, 560), sf::Color::White, 0, 0, true, false, false, false, 0, 0, 0, 0, 60, 200);
	speed_  = new ComplexText(*window_, "Stats", 25, sf::Color::White, "minecraft.ttf", sf::Vector2f(0, 620), sf::Color::White, 0, 0, true, false, false, false, 0, 0, 0, 0, 60, 260);
}

void EndHUD::refresh(int gamemode, int score, int highestCombo, int nbWord, int nbBonCaractere, int nbMauvaisCaractere, int timeInSecond) {

	window_->clear();

	//Score
	score_->setWord("Score : " + std::to_string(score) + " - Meilleur Combo : " + std::to_string(highestCombo));
	score_->draw();

	//Info ecriture
	keyboardstats_->setWord("Nombre de bons et mauvais caracteres : " + std::to_string(nbBonCaractere) + " - " + std::to_string(nbMauvaisCaractere));
	keyboardstats_->draw();

	speed_->setWord("Vitesse d'ecriture : " + std::to_string((float)nbBonCaractere / timeInSecond * 60).substr(0, 5) + " caracteres/min - Nombre de mots: " + std::to_string(nbWord));
	speed_->draw();

	//Texte GameOver
	commentaire_->draw();

	//Bouton quitter
	quitter_->draw();

	//on affiche tout
	window_->display();


}

EndHUD::~EndHUD() {
	//Arr�t de la musique
	sonarriereplan_->stop();

	//Destruction de tous les pointeurs
	delete videoarriereplan_;
	delete score_;
	delete keyboardstats_;
	delete speed_;
	delete commentaire_;
	delete quitter_;
	delete sonarriereplan_;
}