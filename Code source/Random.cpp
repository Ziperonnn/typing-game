// Fonctions Random
// Auteur: Sources sur internet
// Objectif: G�n�rer des nombres al�atoires
//

#include "Random.h"

int randomIntMinMax(int min, int max) {
	int n = max - min + 1;
	int remainder = RAND_MAX % n;
	int x;
	do {
		x = rand();
	} while (x >= RAND_MAX - remainder);
	return min + x % n;
}

int randomInt(int max) {
	int n = max + 1;
	int remainder = RAND_MAX % n;
	int x;
	do {
		x = rand();
	} while (x >= RAND_MAX - remainder);
	return x % n;
}