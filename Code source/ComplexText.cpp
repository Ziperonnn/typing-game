#include "ComplexText.h"

ComplexText::ComplexText(sf::RenderWindow& window, string text, int taille, sf::Color couleur, string font_texte, sf::Vector2f centerPosition, sf::Color couleuroutline, int outlinetickness,  float angle, bool isCenteredOnX, bool isBold, bool isUnderlined, bool animated, float minscale, float maxscale, float fadeduration, float fadedelay, float appearduration, float appeardelay) {
	
	//D�finition des diff�rents param�tres du texte
	speed_ = 0;

	window_ = &window;
	scaletexte_ = 1;
	rising_ = true;
	minscale_ = minscale;
	maxscale_ = maxscale;
	angle_ = angle;
	couleurOutline_ = couleuroutline;
	outlineThick_ = outlinetickness;
	font_.loadFromFile(font_texte);


	textString_ = text;
	taille_ = taille;
	couleur_ = couleur;
	isCentered_ = isCenteredOnX;

	fadeDuration_ = fadeduration;
	fadeState_ = fadeduration;
	fadeDelay_ = fadedelay;

	appearDuration_ = appearduration;
	appearState_ = appearduration;
	appearDelay_ = appeardelay;

	isAnimated_ = animated;

	texte_.setFont(font_);
	texte_.setCharacterSize(taille_);
	texte_.setFillColor(couleur_);
	texte_.setString(textString_);
	texte_.setRotation(angle_);
	texte_.setOutlineColor(couleurOutline_);
	texte_.setOutlineThickness(outlineThick_);

	//Ajout des caract�ristiques au texte (sous lign�, gras)	
	if (isUnderlined && isBold) {
		texte_.setStyle(sf::Text::Bold | sf::Text::Underlined);
	}
	else if (isUnderlined && !isBold) {
		texte_.setStyle(sf::Text::Underlined);
	}
	else if (!isUnderlined && isBold) {
		texte_.setStyle(sf::Text::Bold);
	}
	
	//Ajustement de la position si le texte doit �tre centr�	
	if (isCenteredOnX) {
		if (isAnimated_) {
			texte_.setPosition(window.getSize().x / 2, centerPosition.y);
		} else {
			texte_.setPosition(window.getSize().x / 2 - texte_.getGlobalBounds().width / 2, centerPosition.y);
		}
		
	}
	else {
		texte_.setPosition(centerPosition.x, centerPosition.y);
	}
	
	position_ = texte_.getPosition();
	tailletext_ = texte_.getGlobalBounds();
}

void ComplexText::draw() {
	//Si le texte est anim�
	//Gestion de l'apparition progressive du texte � l'�cran, par alt�ration du channel alpha du texte
	if (appearDuration_ > 1) {
		if (appearDelay_ > 0) {
			appearDelay_--;
			texte_.setOutlineColor(sf::Color(texte_.getOutlineColor().r, texte_.getOutlineColor().g, texte_.getOutlineColor().b, 0));
			texte_.setFillColor(sf::Color(texte_.getFillColor().r, texte_.getFillColor().g, texte_.getFillColor().b, 0));
		}
		else {
			appearDuration_--;
			if (appearDuration_ == 0) {
				texte_.setOutlineColor(sf::Color(texte_.getOutlineColor().r, texte_.getOutlineColor().g, texte_.getOutlineColor().b, 0));
				texte_.setFillColor(sf::Color(texte_.getFillColor().r, texte_.getFillColor().g, texte_.getFillColor().b, 0));
			}
			else {
				int alpha = 255 - appearDuration_ * 255 / appearState_;
				texte_.setOutlineColor(sf::Color(texte_.getOutlineColor().r, texte_.getOutlineColor().g, texte_.getOutlineColor().b, alpha));
				texte_.setFillColor(sf::Color(texte_.getFillColor().r, texte_.getFillColor().g, texte_.getFillColor().b, alpha));
			}			
		}
	}
	//Idem pour la disparition du texte
	else if (fadeDuration_ > 1) {
		if (fadeDelay_ > 0) {
			fadeDelay_--;
		}
		else {
			fadeDuration_--;
			int alpha = fadeDuration_ * 255 / fadeState_;
			texte_.setOutlineColor(sf::Color(texte_.getOutlineColor().r, texte_.getOutlineColor().g, texte_.getOutlineColor().b, alpha));
			texte_.setFillColor(sf::Color(texte_.getFillColor().r, texte_.getFillColor().g, texte_.getFillColor().b, alpha));
		}
	}
	else if (fadeDuration_ == 1) {
		texte_.setOutlineColor(sf::Color(texte_.getOutlineColor().r, texte_.getOutlineColor().g, texte_.getOutlineColor().b, 0));
		texte_.setFillColor(sf::Color(texte_.getFillColor().r, texte_.getFillColor().g, texte_.getFillColor().b, 0));
	}

	texte_.setString(textString_);

	//Si le texte est anim� (grossissement puis r�tr�cissement)
	if (isAnimated_) {
		float risingspeed = (maxscale_ - minscale_) / 60; //Nous sommes sur 60FPS, on calcule la vitesse de variation
		texte_.setScale(scaletexte_, scaletexte_);
		if (rising_) {
			scaletexte_ = scaletexte_ + risingspeed;
			if (scaletexte_ > maxscale_) {
				rising_ = !rising_;
			}
		}
		else {
			scaletexte_ = scaletexte_ - risingspeed;
			if (scaletexte_ < minscale_) {
				rising_ = !rising_;
			}
		}
		tailletext_ = texte_.getGlobalBounds();
		int xpos = position_.x - tailletext_.width / 2;
		texte_.setPosition(xpos, texte_.getPosition().y); //Repositionnement du texte apr�s variation de la taille
	}
	
	window_->draw(texte_); //Actualisation du texte � l'�cran
}

bool ComplexText::isClicked(int x, int y) {

	//Verification si l'endroit cliqu� est sur le texte
	if (x > tailletext_.left && x < (tailletext_.left + tailletext_.width) && y > tailletext_.top && y < (tailletext_.top + tailletext_.height)) {
		//Bouton cliqu�!
		return true;
	}
	return false;
}

void ComplexText::setWord(string word) {
	textString_ = word;
	position_ = texte_.getPosition();
	//On repositionne le texte vu que la taille du texte a probablement chang�
	if (isCentered_) {
		if (isAnimated_) {
			texte_.setPosition(window_->getSize().x / 2, position_.y);
		}
		else {
			texte_.setPosition(window_->getSize().x / 2 - texte_.getGlobalBounds().width / 2, position_.y);
		}

	}
	else {
		texte_.setPosition(position_.x, position_.y);
	}
}

void ComplexText::instantFade(int duration) {
	fadeDuration_ = duration;
	fadeState_ = duration;
}

void ComplexText::setColor(sf::Color couleur) {
	couleur_ = couleur;
	texte_.setFillColor(couleur_);
}