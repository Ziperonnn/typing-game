#define _CRT_SECURE_NO_DEPRECATE
#include "AccueilHUD.h"

AccueilHUD::AccueilHUD(sf::RenderWindow& window) {
	window_ = &window;

	const string version = "a1.2.2";

	//Initialisation de la vid�o en arri�re plan
	videoarriereplan_ = new VideoLoader(window, "video.avi", window.getSize().x, window.getSize().y, 0, 0, true);

	//Initialisation des diff�rents textes
	titlemain_ = new ComplexText(window, "Dactylo Fall", 150, sf::Color::White, "neonavy.ttf", sf::Vector2f(0, 10), sf::Color::White, 0, 0, true, false, false, false);
	titleversion_ = new ComplexText(window, "Version " + version, 60, sf::Color::Yellow, "neoncity.ttf", sf::Vector2f(0, 180), sf::Color::White, 0, 0, true, false, true, false);
	titlesplash_ = new ComplexText(window, "B]1", 130, sf::Color(255, 102, 255), "katakana.ttf", sf::Vector2f(1720, 130), sf::Color::White, 0, -30, false, false, false, true, 1.0, 1.3);

	//Initialisation du texte affich� lors du d�marrage
	presents_ = new ComplexText(window, "Pr�sentent", 40, sf::Color(1, 0, 0, 0), "neonavy.ttf", sf::Vector2f(0, 430), sf::Color::White, 0, 0, true, false, true, false);
	pierreetloic_ = new ComplexText(window, "Pierre & Loic", 60, sf::Color(1, 0, 0, 0), "neonavy.ttf", sf::Vector2f(0, 310), sf::Color::White, 0, 0, true, false, false, true, 1.0, 1.05);

	
	
	
	//V�rification de la pr�sence du fichier w.txt qui indique que la fonctionnalit� cach�e est activ�e
	//Initialisation de la musique de fond
	FILE* file;
	if (file = fopen("w.txt", "r")) {
		fclose(file);
		sonarriereplan_ = new SoundLoader("mainmenuweeb.wav", 50, true, 0, 0, 0, true);
	}
	else {
		sonarriereplan_ = new SoundLoader("mainmenu.wav", 50, true, 0, 0, 0, true);
	}
	


	//Initialisation des boutons
	normal_ = new ComplexText(window, "Normal", 60, sf::Color(255, 155, 217), "neonavy.ttf", sf::Vector2f(0, 310), sf::Color::White, 2, 0, true, false, false, true, 1.0, 1.05);
	difficile_ = new ComplexText(window, "Difficile", 60, sf::Color::Magenta, "neonavy.ttf", sf::Vector2f(0, 410), sf::Color::White, 2, 0, true, false, false, true, 1.0, 1.05);
	chaos_ = new ComplexText(window, "CHAOS", 60, sf::Color(254, 20, 105), "neonavy.ttf", sf::Vector2f(0, 510), sf::Color::White, 2, 0, true, false, false, true, 1.0, 1.05);
	quitter_ = new ComplexText(window, "Quitter", 50, sf::Color::Red, "neonavy.ttf", sf::Vector2f(1620, 1000), sf::Color::White, 2, 0);
	novideo_ = new ComplexText(window, "Vid�os activ�es", 50, sf::Color(254, 20, 105), "neonavy.ttf", sf::Vector2f(0, 750), sf::Color::White, 2, 0, true, false, false, false);

	alpha_ = -100;
}

AccueilHUD::~AccueilHUD() {
	//Arr�te de la musique
	sonarriereplan_->stop();

	//Destruction de l'ensemble des pointeurs
	delete videoarriereplan_;
	delete sonarriereplan_;

	delete titlemain_;
	delete titleversion_;
	delete titlesplash_;
	delete normal_;
	delete difficile_;
	delete chaos_;
	delete quitter_;
	delete novideo_;


	delete presents_;
	delete pierreetloic_;
}

void AccueilHUD::refresh(bool booting, bool video){
	//On supprime tout ce qui est pr�sent dans la fen�tre
	window_->clear();

	//Si nous nous trouvons dans la boucle de d�marrage, on g�re l'apparition du texte avant l'affichage du menu
	if (booting && alpha_ < 1500) {
		if (alpha_ > 0 && alpha_ < 256) {
			pierreetloic_->setColor(sf::Color(255, 155, 217, alpha_));
		}
		if (alpha_ > 0 && alpha_ < 1000) {
			presents_->setColor(sf::Color(255, 255, 0, alpha_/4));
						}

		if (alpha_ > 1500) {
			booting = false;
		}
		if (alpha_ > 255) {
			alpha_ = alpha_ + 25;
		}
		else {
			alpha_ = alpha_ + 1;
		}

		
		//On affiche le texte de d�marrage
		pierreetloic_->draw();
		presents_->draw();
	}
	else {
		//On affiche tout ce qu'il faut � l'�cran
		videoarriereplan_->draw();
		titlemain_->draw();
		titleversion_->draw();
		titlesplash_->draw();

		normal_->draw();
		difficile_->draw();
		chaos_->draw();
		quitter_->draw();
		//En fonction de si la vid�o est activ�e ou non, on affiche le bouton en cons�quent
		if (video) {
			novideo_->setWord("Vid�o activ�e");
		}
		else {
			novideo_->setWord("Vid�o d�sactiv�e");
		}
		novideo_->draw();

	
	}
	//On affiche tout
	window_->display();
}