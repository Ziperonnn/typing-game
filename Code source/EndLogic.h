#pragma once
#include <SFML/Graphics.hpp>
#include <sfeMovie/Movie.hpp>
#include <sfeMovie/Visibility.hpp>
#include <sfeMovie/StreamSelection.hpp>
#include <iostream>
#include <SFML\Audio\Music.hpp>
#include <string>
#include "WordsListener.h"
#include "EndHUD.h"

class EndLogic {
private:

	sf::RenderWindow* window_;
	EndHUD* hud_;


	int vie_;

	int score_;
	int combo_;
	int highestCombo_;
	int nbWord_;

	int nbBonCaractere_;
	int nbMauvaisCaractere_;
	int timeInSecond_;
	int nbBonCaractere_;
	int nbMauvaisCaractere_;
	int timeInSecond_;
	int gameMode_;

	bool isPlaying_;

public:
	//GameLogic() {};
	EndLogic(sf::RenderWindow& window, int gamemode = 0, int score = 0, int highestCombo = 0, int nbWord = 0, int nbBonCaractere = 0, int nbMauvaisCaractere = 0, int timeInSecond = 0);
	void refresh();
	~EndLogic();
};
