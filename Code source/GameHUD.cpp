#define _CRT_SECURE_NO_DEPRECATE
#include "GameHUD.h"

GameHUD::GameHUD(sf::RenderWindow& window) {
	window_ = &window;
	
	//Initialisation de la vid�o d'arri�re plan	
	videoarriereplan_ = new VideoLoader(*window_, "arriereplanjeu.mp4", window_->getSize().x, window_->getSize().y, 0, 0, true);
	
	FILE* file;
	
	//On v�rifie si le fichier w.txt existe, si c'est le cas, on joue la musique cach�e, sinon, on joue la musique de base
	if (file = fopen("w.txt", "r")) {
		fclose(file);
		//Il y a deux musique cach�e, il y a 50% de chance de tomber sur l'une comme sur l'autre
		if (randomInt(100) < 50) {
			sonarriereplan_ = new SoundLoader("gameweeb1.wav", 5, true, 0, 0, 0, true);
		}
		else {
			sonarriereplan_ = new SoundLoader("gameweeb2.wav", 5, true, 0, 0, 0, true);
		}
		
	}
	else {
		sonarriereplan_ = new SoundLoader("game.wav", 5, true, 0, 0, 0, true);
	}
	
	//On v�rifie si le fichier v.txt existe, si c'est le cas, on met video_ � true, cela indique qu'il faudra jouer la vid�o en fond.
	if (file = fopen("v.txt", "r")) {
		fclose(file);
		video_ = true;

	}
	else {
		video_ = false;
	}

	//On initialise le son de f�licitation
	sonaction_ = new SoundLoader("correct.wav", 5);

	//Initialisation du bouton quitter
	quitter_ = new ComplexText(*window_, "Quitter", 30, sf::Color::Red, "neonavy.ttf", sf::Vector2f(1750, 1030), sf::Color::White, 2, 0);

	//Initialisation du score en haut de l'�cran
	score_ = new ComplexText(*window_, "Score", 25, sf::Color::White, "neonavy.ttf", sf::Vector2f(1661, 10), sf::Color::White, 0, 0);

	//Texte et texture affich�e � c�t� d'un mot d�truit
	commentaire_ = new ComplexText(*window_);
	texture_ = new TextureLoader(*window_);


	//Initialisation des textures de la barre de vie
	texturevie_.loadFromFile("heart.png");
	texturevievide_.loadFromFile("heartempty.png");

	vie_sprite1.setTexture(texturevie_);
	vie_sprite1.setScale(0.09, 0.09);
	vie_sprite1.setPosition(10, 2);
	vie_sprite2.setTexture(texturevie_);
	vie_sprite2.setScale(0.09, 0.09);
	vie_sprite2.setPosition(90, 2);
	vie_sprite3.setTexture(texturevie_);
	vie_sprite3.setScale(0.09, 0.09);
	vie_sprite3.setPosition(170, 2);

	//On charge �galement l'image de fond
	imagearriereplan_.loadFromFile("arriereplanjeu.png");
	//On la place ensuite sur l'�cran
	spritearriereplan_.setTexture(imagearriereplan_);
	spritearriereplan_.setScale((float) window_->getSize().x / 1847, (float) window_->getSize().y / 1005);
}

void GameHUD::refresh(int vie, int score, int combo, int bestCombo, vector<ComplexText*>* wordsList) {

	window_->clear();
	//On red�finie la vie
	setVie(vie);

	//Si on doit afficher la vid�o, on l'actualise
	if (video_) {
		videoarriereplan_->draw();
	}
	else {
		//Sinon, on affiche l'image de fond
		window_->draw(spritearriereplan_);
	}
	
	

	//Score
	score_->setWord("Score : " + std::to_string(score) + "\nCombo : " + std::to_string(combo) + "\nBC : " + std::to_string(bestCombo));
	score_->draw();

	//Vie
	window_->draw(vie_sprite1);
	window_->draw(vie_sprite2);
	window_->draw(vie_sprite3);

	//On affiche les mots d�filants sur l'�cran
	//On int�re le vecteur dans l'autre sens pour affiche le mot qui doit �tre tap� � la fin
	//Cela permet d'�viter que celui-ci soit cach� derri�re un autre
	//Le mot que le joueur doit �crire sera rouge
	vector<ComplexText*>::reverse_iterator it = wordsList->rbegin();
	int count = wordsList->size()-1;
	int Vectorposition = -1;
	float Yposition = 0;
	for (it; it != wordsList->rend(); it++) {
		if ((*it)->getY() > 0) {
			(*it)->draw();
			if (wordsList->at(count)->getText().getFillColor() == sf::Color::Red) { //On remet tous les mots en bleu
				wordsList->at(count)->getText().setFillColor(sf::Color(0, 100, 252));
			}
			count--;
		}

	}
	//On met ensuite le mot que le joueur doit �crire en rouge
	if (wordsList->size() > 0) wordsList->at(0)->getText().setFillColor(sf::Color::Red);

	//Commentaire destruction de mots et sa texture
	commentaire_->draw();
	texture_->draw();

	//Bouton quitter
	quitter_->draw();

	window_->display();

	
}

void GameHUD::setVie(int vie) {
	//En fonction de la vie, on doit afficher des coeurs pleins ou des coeurs vides
	switch (vie) {
	case 1:
		vie_sprite3.setTexture(texturevievide_);
		vie_sprite2.setTexture(texturevievide_);
		vie_sprite1.setTexture(texturevie_);
		break;
	case 2:
		vie_sprite3.setTexture(texturevievide_);
		vie_sprite2.setTexture(texturevie_);
		vie_sprite1.setTexture(texturevie_);
		break;
	case 3:
		vie_sprite3.setTexture(texturevie_);
		vie_sprite2.setTexture(texturevie_);
		vie_sprite1.setTexture(texturevie_);
		break;
	case 0:
		vie_sprite3.setTexture(texturevievide_);
		vie_sprite2.setTexture(texturevievide_);
		vie_sprite1.setTexture(texturevievide_);
		break;
		
	}
}

GameHUD::~GameHUD(){
	//Le destructeur va cr�er un effet de distorsion de la musique, comme si celle-ci ralentissait.
	float pitch = 1;
	sf::Clock clock;
	while (clock.getElapsedTime().asSeconds() < 3)
	{
		if ((clock.getElapsedTime().asMilliseconds() % 100) == 0) {
			sonarriereplan_->setPitch(pitch);
			if (pitch > 0.01) pitch = pitch - 0.00001;
		}
	}
	//Apr�s cela, on d�tuit l'objet
	//On arr�te la musique
	sonarriereplan_->stop();

	//On d�salloue l'ensemble des pointeurs
	delete videoarriereplan_;
	delete score_;
	delete sonaction_;
	delete commentaire_;
	delete texture_;
	delete quitter_;
	delete sonarriereplan_;
}