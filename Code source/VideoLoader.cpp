#include "VideoLoader.h"

VideoLoader::VideoLoader(sf::RenderWindow& window, string nomVideo, float xscale, float yscale, int xpos, int ypos, bool isRepeated) {
	window_ = &window;

	isRepeated_ = isRepeated;

	//Ouvrir le fichier de la vid�o
	movie_.openFromFile(nomVideo);

	//Positionner la vid�o
	movie_.fit(xpos, ypos, xscale, yscale);
	
	//Jouer la vid�o
	movie_.play();
}

void VideoLoader::setScale(float xscale = 1.0, float yscale = 1.0) {
	movie_.setScale(xscale, yscale);
}

void VideoLoader::setPosition(int xpos = 0, int ypos = 0) {
	movie_.setPosition(xpos, ypos);
}

void VideoLoader::draw() {
	if (movie_.getStatus() == 0) { //si on est arriv� au bout de la vid�o
		if (isRepeated_) { //Et qu'on a choisi de la lire en boucle
			movie_.play(); //On la red�marre
		}
	}
	else {
		movie_.update(); //On r�actualise l'affichage
		window_->draw(movie_); //On envoie l'affichage actualis� � la fen�tre
	}
	
}