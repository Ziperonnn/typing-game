// Class Dictionnaire
// Auteur: Pierre TASSAN
// Objectif: G�rer la lecture des mots d'un fichier et leur tirage al�atoire
//
#pragma once
#include <SFML/Graphics.hpp>
#include <sfeMovie/Movie.hpp>
#include <sfeMovie/Visibility.hpp>
#include <sfeMovie/StreamSelection.hpp>
#include <iostream>
#include <SFML\Audio\Music.hpp>
#include <string>
#include <fstream>
#include <vector>

#include "Random.h"

using namespace std;

class Dictionnaire {
private:
	//vecteur contenant tous les mots
	vector<string> listeMots_;
	//taille de ce vecteur
	int nbMots_;

public:
	//Contructeur prenant en param�tre le nom du fichier contenant les mots
	Dictionnaire(string nomDictionnaire);

	//Retourne un mot al�atoire de la liste
	string askWord() const;
};