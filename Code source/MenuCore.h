// Class MenuCore
// Auteur: Pierre TASSAN et Lo�c Chamberlin
// Objectif: G�rer la transition entre le menu principal et le menu de jeu
//
#pragma once
#include <SFML/Graphics.hpp>
#include <sfeMovie/Movie.hpp>
#include <sfeMovie/Visibility.hpp>
#include <sfeMovie/StreamSelection.hpp>
#include <SFML\Audio\Music.hpp>
#include "GameLogic.h"
#include "AccueilLogic.h"

class MenuCore {
private:
	//R�f�rence � la fen�tre pour pouvoir g�rer l'affichage
	sf::RenderWindow* window_;
public:
	//Constructeur
	//Prend en param�tre la fen�tre pour ensuite la donner � son GameHUD (puis EndHUD)
	MenuCore(sf::RenderWindow &fenetre);

	//Permet de lancer une partie avec une difficult� pass�e en param�tre
	void MenuGame(int gamemode);

	//Affiche le menu principal
	int MenuAccueil(bool booting);
};