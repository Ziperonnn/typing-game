// Class AccueilHUD
// Auteur: Pierre TASSAN et Lo�c Chamberlin
// Objectif: G�rer l'affichage graphique du menu principal
//

#pragma once
#include <SFML/Graphics.hpp>
#include <sfeMovie/Movie.hpp>
#include <sfeMovie/Visibility.hpp>
#include <sfeMovie/StreamSelection.hpp>
#include <iostream>
#include <SFML\Audio\Music.hpp>

#include "VideoLoader.h"
#include "TextureLoader.h"
#include "ComplexText.h"
#include "SoundLoader.h"

class AccueilHUD {
private:
	//R�f�rence � la fen�tre pour pouvoir g�rer l'affichage
	sf::RenderWindow* window_;

	//Vid�o et son d'arri�re plan
	VideoLoader* videoarriereplan_;
	SoundLoader* sonarriereplan_;

	//Diff�rents textes de l'�cran
	ComplexText* titlemain_;
	ComplexText* titleversion_;
	ComplexText* titlesplash_;
	ComplexText* normal_;
	ComplexText* difficile_;
	ComplexText* chaos_;
	ComplexText* quitter_;
	ComplexText* novideo_;

	//Texte de l'opening
	ComplexText* presents_;
	ComplexText* pierreetloic_;

	float alpha_;

public:
	//Constructeur
	//Ne prend pour param�tre que la fen�tre
	AccueilHUD(sf::RenderWindow& window);

	~AccueilHUD();

	//Fonction appel�e � chaque actualisation, permet de mettre � jour l'affichage sur l'�cran du menu principal
	void refresh(bool booting = true, bool video = true);

	//Permet de retourner le pointeur des diff�rents textes affich�s � l'�cran, utilis� dans AccueilLogic pour d�terminer si des boutons ont �t� cliqu�s
	ComplexText* getNormalButton() { return normal_; };
	ComplexText* getDifficileButton() { return difficile_; };
	ComplexText* getChaosButton() { return chaos_; };
	ComplexText* getQuitButton() { return quitter_; };
	ComplexText* getVideoButton() { return novideo_; };

	//Idem pour le petit texte en haut � droite de l'�cran, permet d'activer la fonctionnalit� cach�e
	ComplexText* getSplashButton() { return titlesplash_; };
};