// Class SoundLoader
// Auteur: Pierre TASSAN
// Objectif: G�rer de mani�re simplifi�e la lecture de fichiers son
//

#pragma once
#include <SFML/Graphics.hpp>
#include <sfeMovie/Movie.hpp>
#include <sfeMovie/Visibility.hpp>
#include <sfeMovie/StreamSelection.hpp>
#include <iostream>
#include <SFML/Audio/Music.hpp>
#include <string>

using namespace std;

class SoundLoader {
private:
	//Objet SFML de la musique
	sf::Music music_;
public:
	//Constructeur
	//Prend de nombreux param�tres 
	//string nomSon = ""              Nom du fichier contenant la musique
	//int volume = 5                  Le volume de la musique
	//bool loop = false               Si la musique doit boucler (red�marrer quand elle est fini)
	//float xpos = 0                  Spacialisation 3D de la musique
	//float ypos = 0
	//float zpos = 0
	//bool play = false              Si la musique doit d�marrer d�s que l'objet est instanci�
	SoundLoader(string nomSon = "", int volume = 5, bool loop = false, float xpos = 0, float ypos = 0, float zpos = 0, bool play = false);
	
	//Lancer la musique
	void play() { music_.play(); };

	//Arr�ter et revenir au d�but de la musique
	void stop() { music_.stop(); };

	//Mettre la musique en pause
	void pause() { music_.pause(); };

	//Changer le pitch de la musique (donne un effet de jouer la musique au ralenti)
	void setPitch(float pitch) { music_.setPitch(pitch); };
};