// Class WordsListener
// Auteur: Pierre TASSAN
// Objectif: G�rer la saisie des mots � l'�cran, leur cr�ation, leur destruction et l'envoie des informations � GameLogic
//
#pragma once
#include <SFML/Graphics.hpp>
#include <sfeMovie/Movie.hpp>
#include <sfeMovie/Visibility.hpp>
#include <sfeMovie/StreamSelection.hpp>
#include <iostream>
#include <SFML\Audio\Music.hpp>
#include <string>

#include "ComplexText.h"
#include"Dictionnaire.h"

using namespace std;

class WordsListener {
private:
	//R�f�rence vers la fen�tre o� afficher la vid�o
	sf::RenderWindow* window_;

	//Vecteur contenant la liste des mots � afficher � l'�cran
	vector<ComplexText*> fallingWordOnScreen_;

	//Nombre de mots dans le vecteur
	int nbWordOnScreen_;

	//Composition: Dictionnaire permettant de retourner des mots � afficher
	Dictionnaire dictionnaire_;

	//Position du dernier mot d�truit
	sf::Vector2f lastWord_;

	//Mode de jeu
	int gamemode_;

public:
	//Constructeur
	//Prend en param�tre un pointeur vers la fen�tre et le mode de jeu
	WordsListener(sf::RenderWindow& window, int gamemode = 0);

	//Destructeur
	~WordsListener();

	//Permet d'entrer une touche et v�rifier si celle-ci est la bonne
	//Retours possibles:
	// 0 Mauvaise touche
	// 1 Bonne touche
	// 2 Mot d�truit
	// 3 Mot Bonus d�truit
	int typedKey(char key);

	// Permet de rajouter un autre mot sur l'�cran avec une vitesse
	// Appel� par GameLogic
	void addWord(float speed);

	//Permet de mettre � jour les coordonn�es des mots
	//Donc de les faire d�filer sur l'�cran
	bool updateWords();

	//Retourne le nombre de mots sur l'�cran
	int nbWord() const { return nbWordOnScreen_; };

	//Retourne un pointeur contenant la liste des mots et leurs coordonn�es
	//Utilis� par GameHUD pour afficher les mots sur l'�cran
	vector<ComplexText*> * returnWords() { return &fallingWordOnScreen_;};

	//Retour les coordonn�es du derni�re mot d�truit
	sf::Vector2f lastDestuctionLocation() const { return lastWord_; };
};