// Class ComplexText
// Auteur: Pierre TASSAN
// Objectif: G�rer de mani�re simplifi�e l'affichage des textes en ajoutant �galement des effets d'animation � ceux-ci (rotation, variation de taille, apparition et disparition en fondu, d�placements, ...)
//

#pragma once
#include <SFML/Graphics.hpp>
#include <sfeMovie/Movie.hpp>
#include <sfeMovie/Visibility.hpp>
#include <sfeMovie/StreamSelection.hpp>
#include <iostream>
#include <SFML\Audio\Music.hpp>
#include <string>
#include "ComplexTextAnimator.h"

using namespace std;

class ComplexText {
private:
	//R�f�rence � la fen�tre pour pouvoir g�rer l'affichage
	sf::RenderWindow* window_;

	//Tous les param�tres du texte
	sf::Text texte_;
	int taille_;
	sf::Color couleur_;
	sf::Color couleurOutline_;
	int outlineThick_;
	sf::Font font_;
	string textString_;
	sf::Vector2f position_;
	float angle_;
	sf::FloatRect tailletext_;
	float scaletexte_;
	float isCentered_; //Si le texte se situe au centre de l'�cran (axe horizontal)

	//Relatif � l'animation du texte
	bool rising_;
	bool isAnimated_;
	float maxscale_;
	float minscale_;
	

	int fadeDelay_;
	int fadeDuration_;
	int fadeState_;

	int appearDelay_;
	int appearDuration_;
	int appearState_;

	float speed_;

public:
	//Constructeur
	//Prend de nombreux param�tres en compte:
	//sf::RenderWindow& window                                  Pointeur vers la fen�tre pour g�rer l'affichage
	//string text = ""                                          Texte � afficher
	//int taille = 0                                            Taille du texte
	//sf::Color couleur = sf::Color::White                      Couleur de l'int�rieur du texte
	//string font_texte = ""                                    Nom du fichier de la police d'�criture
	//sf::Vector2f centerPosition = sf::Vector2f(0,0)           Position du centre du texte sur l'�cran
	//sf::Color outline = sf::Color::White                      Couleur du contour du texte
	//int outlinetickness = 0                                   Epaisseur des contours
	//float angle = 0                                           Angle du texte
	//bool isCenteredOnX = false                                Est ce que le texte doit �tre centre au milieu de l'�cran sur l'axe horizontal
	//bool isBold = false                                       Est ce que le texte est en gras
	//bool isUnderlined = false                                 Est ce que le texte est sous-lign�
	//bool animated = false                                     Est ce que le texte a un animation (grossissement puis r�tr�cissement)
	//float minscale = 1.0                                      Si oui, la taille qu'il atteint lors du r�tr�cissement
	//float maxscale = 1.3                                      Si oui, la taille qu'il atteint lors du grossissement 
	//float fadeduration = 0                                    Est ce que le texte disparait au bout d'un certain temps (valeur en frames �coul�es)
	//float fadedelay = 0                                       Si oui, combien de temps dure la disparition
	//float appearduration = 0                                  Est ce que le texte met un certain temps � apparaitre
	//float appeardelay = 0                                     Si oui, combien de temps dure l'apparition
	ComplexText(sf::RenderWindow& window, string text = "", int taille = 0, sf::Color couleur = sf::Color::White, string font_texte = "", sf::Vector2f centerPosition = sf::Vector2f(0,0), sf::Color outline = sf::Color::White, int outlinetickness = 0, float angle = 0, bool isCenteredOnX = false, bool isBold = false, bool isUnderlined = false, bool animated = false, float minscale = 1.0, float maxscale = 1.3, float fadeduration = 0, float fadedelay = 0, float appearduration = 0, float appeardelay = 0);
	
	//Utilis� pour actualiser l'affichage du texte � l'�cran
	void draw();
	
	//Permet de retourner true si l'utilisateur a cliqu� sur ce texte
	bool isClicked(int x, int y);
	
	//Permet de faire descendre le texte sur l'�cran, utile pendant la partie
	void addY(int adder) { texte_.setPosition(texte_.getPosition().x, texte_.getPosition().y + adder); };
	
	//Permet de d�clencher manuellement la disparition du texte de l'�cran
	void instantFade(int duration);
	
	//Retourner la vitesse de d�placement du texte sur l'�cran
	float getSpeed() const { return speed_; };

	//Retourne une r�f�rence vers le sf::text
	sf::Text& getText() { return texte_; };

	//Retourne la position du texte
	sf::Vector2f getLocation() const { return texte_.getPosition(); };

	//Retourne un string contenant le texte de ce texte
	string getWord() const { return textString_; };

	//Retourner la position en Y de ce texte
	float getY() const { return texte_.getPosition().y; };

	//Permet de d�finir la vitesse de d�placement du texte
	void setSpeed(float speed) { speed_ = speed; };

	//Permet de changer le texte affich�
	void setWord(string word);

	//Permet de changer la couleur de l'int�rieur du texte
	void setColor(sf::Color couleur);
};
