// Class GameHUD
// Auteur: Pierre TASSAN et Lo�c Chamberlin
// Objectif: G�rer l'affichage graphique de la partie
//
#pragma once
#include <SFML/Graphics.hpp>
#include <sfeMovie/Movie.hpp>
#include <sfeMovie/Visibility.hpp>
#include <sfeMovie/StreamSelection.hpp>
#include <iostream>
#include <SFML\Audio\Music.hpp>

#include "VideoLoader.h"
#include "TextureLoader.h"
#include "ComplexText.h"
#include "SoundLoader.h"
#include "Random.h"

class GameHUD {
private:
	//R�f�rence � la fen�tre pour pouvoir g�rer l'affichage
	sf::RenderWindow* window_;

	//Vid�o et son d'arri�re plan
	VideoLoader * videoarriereplan_;
	ComplexText* score_;

	//Image d'arri�re plan dans le cas ou nous avons d�sactiv� la vid�o
	sf::Sprite spritearriereplan_;
	sf::Texture imagearriereplan_;

	//Diff�rents textures et sprits de la barre de vie
	sf::Texture texturevie_;
	sf::Texture texturevievide_;
	sf::Sprite vie_sprite1;
	sf::Sprite vie_sprite2;
	sf::Sprite vie_sprite3;

	
	
	//Diff�rents textes
	ComplexText* commentaire_;
	TextureLoader* texture_;
	ComplexText* quitter_;
	SoundLoader* sonarriereplan_;

	//Son jou� quand un joueur complet un mot bonus
	SoundLoader* sonaction_;

	//Indique si la vid�o de fond doit �tre jou�e
	bool video_;

public:
	//Constructeur
	//Ne prend pour param�tre que la fen�tre
	GameHUD(sf::RenderWindow& window);

	//Destructeur
	~GameHUD();

	//Fonction appel�e � chaque actualisation, permet de mettre � jour l'affichage sur l'�cran de jeu
	//Prend en param�tre diff�rentes informations fournies par GameLogic
	//int vie                              La vie du joueur
	//int score                            Le score du joueur
	//int combo                            Le combo actuel du joueur
	//int bestCombo                        Le meilleur combo du joueur
	//vector<ComplexText*>* wordsList      Un vecteur contenant la liste des mots actuellement affich�s � l'�cran
	void refresh(int vie, int score, int combo, int bestCombo, vector<ComplexText*>* wordsList);
	
	//Permet de modifier la vie affich�e � l'�cran
	void setVie(int vie);

	//Permet d'affiche un texte � l'endroit du dernier mot d�truit
	void setCommentaire(ComplexText* texte) { commentaire_ = texte; };
	//Permet d'affiche une image � l'endroit du dernier mot d�truit
	void setTexture(TextureLoader* texture) { texture_ = texture; };

	//Permet de jouer un son (lors de la destruction d'un mot par le joueur)
	void setSound(SoundLoader* sound) { sonaction_ = sound; };

	//Retourne un pointeur vers le son jou�
	SoundLoader* getSound() { return sonaction_; };

	//Retourne un pointeur vers le bouton quitt�, permet � GameLogic de v�rifier si le joueur � cliquer sur ce bouton
	ComplexText* getQuitButton() { return quitter_; };

};