#include "WordsListener.h"

WordsListener::WordsListener(sf::RenderWindow& window, int gamemode) : dictionnaire_("dico.txt") {
	srand(time(NULL)); //D�finit le seed de l'al�atoire
	gamemode_ = gamemode;
	window_ = &window;
}

WordsListener::~WordsListener() {
	//Le destructeur d�salloue chaque mots stock�s dans le vecteur
	for (unsigned int i = 0; i < fallingWordOnScreen_.size(); i++) {
		delete fallingWordOnScreen_[i];
	}
}


int WordsListener::typedKey(char key) {
	vector<ComplexText *>::iterator it = fallingWordOnScreen_.begin();

	//Le mot qui doit �tre tap� est � l'indice 0 du vecteur
	if (fallingWordOnScreen_.at(0)->getWord() != "") { //Si le mot n'est pas d�truit

		bool validation = false;

		if (fallingWordOnScreen_.at(0)->getWord().front() == key) validation = true; //Si la touche tap� et bien la lettre du mot
		if (tolower(fallingWordOnScreen_.at(0)->getWord().front()) == key && gamemode_ == 0) validation = true; //Idem mais en mode facile, pas besoin de majuscule

		if (validation) { //Si la touche est donc bien la bonne
			fallingWordOnScreen_.at(0)->setWord(fallingWordOnScreen_.at(0)->getWord().substr(1)); //On supprime la lettre du mot
			
			if (fallingWordOnScreen_.at(0)->getWord() == "") { //Si le mot est completement d�truit
				lastWord_ = fallingWordOnScreen_.at(0)->getLocation(); //On sauvegarde sa position
				int isbonus = 2;
				if (fallingWordOnScreen_.at(0)->getText().getOutlineColor() == sf::Color(255, 255, 0)) { //On v�rifie si le mot �tait un mot bonus
					isbonus = 3;
				}
				delete fallingWordOnScreen_.at(0); //On supprime le pointeur du mot
				fallingWordOnScreen_.erase(fallingWordOnScreen_.begin() + 0); //On supprime le mot du vecteur
				return isbonus;
			}
			else {
				return 1;
			}
		}
		else {
			return 0;
		}
	}
}

bool WordsListener::updateWords() {
	nbWordOnScreen_ = 0;
	vector<ComplexText *>::iterator it = fallingWordOnScreen_.begin();
	for (it; it != fallingWordOnScreen_.end(); it++) { //On it�re tous les mots
		if ((*it)->getY() > 0) {
			if ((*it)->getY() > window_->getSize().y - 2) { //Si le mot est d�scendu tout en bas de l'�cran
				//Mot rat�
				delete fallingWordOnScreen_.at(nbWordOnScreen_); //On lib�re le pointeur
				fallingWordOnScreen_.erase(it); //On le supprime et on enl�vera ensuite une vie via le return
				return true;
			}
			(*it)->addY((*it)->getSpeed()); //On fait d�filer le mot en fonction de sa vitesse
			nbWordOnScreen_++;
		}
	}
	return false;
}

void WordsListener::addWord(float speed) {
	
	string word = dictionnaire_.askWord(); //On r�cup�re un mot al�atoire de la classe dictionnaire
	if (gamemode_ == 3) { //Si on est en mode Chaos, le mot doit obligatoirement faire plus de 5 lettres, donc si ce n'est pas le cas, on redemande un autre mot (tant que la condition n'est pas respect�e).
		while (word.length() < 6) {
			word = dictionnaire_.askWord();
		}
	}
	


	ComplexText* text; //On cr�e le nouveau texte
	if (randomInt(100) < 10) { //Chance de 10% que le mot soit un mot bonus
		text = new ComplexText(*window_, word, 50, sf::Color(0, 100, 252), "scratch.ttf", sf::Vector2f(randomIntMinMax(300, window_->getSize().x - 300), 1), sf::Color(255,255,0), 5, 0);

	}
	else {
		text = new ComplexText(*window_, word, 50, sf::Color(0, 100, 252), "scratch.ttf", sf::Vector2f(randomIntMinMax(300, window_->getSize().x - 300), 1), sf::Color::White, 5, 0);

	}

	//On d�fini sa vitesse
	text->setSpeed(speed);

	//On l'ajoute au vecteur
	fallingWordOnScreen_.push_back(text);
}
