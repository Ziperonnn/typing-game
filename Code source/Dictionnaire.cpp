#include "Dictionnaire.h"

Dictionnaire::Dictionnaire(string nomDictionnaire) {
    FILE* dictionnaire;

    string line;

    
    ifstream file(nomDictionnaire);
    if (file.fail()) {
        cout << "Impossible de trouver le fichier du dictionnaire!" << endl;
        system("pause");
    }
    else {
        while (std::getline(file, line)) { //On calcule le nombre de mots dans le dictionnaire
            nbMots_++;
        }
        file.close();


        listeMots_ = vector<string>(nbMots_); //On initialise le vecteur

        int count = 0;
        file.open(nomDictionnaire);
        while (std::getline(file, line)) {
            listeMots_.at(count) = line; //On remplit le vecteur
            count++;
        }
    }
}



string Dictionnaire::askWord() const {
    return listeMots_.at(randomInt(nbMots_)); //On retourne un mot al�atoire du vecteur
}