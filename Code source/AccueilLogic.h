// Class AccueilLogic
// Auteur: Pierre TASSAN
// Objectif: G�rer la logique derri�re le menu d'accueil du jeu
//
#pragma once
#include <SFML/Graphics.hpp>
#include <sfeMovie/Movie.hpp>
#include <sfeMovie/Visibility.hpp>
#include <sfeMovie/StreamSelection.hpp>
#include <iostream>
#include <SFML\Audio\Music.hpp>
#include <string>
#include "AccueilHUD.h"
#include "GameLogic.h"

class AccueilLogic {
private:
	//R�f�rence � la fen�tre pour pouvoir g�rer l'affichage
	sf::RenderWindow* window_;
	//Pointeur vers l'affichage graphique de l'accueil sous forme d'agr�gation
	AccueilHUD* hud_;
	
	//D�termine si on � choisi de mettre les vid�os de fond ou pas
	bool video_;
public:
	//Constructeur
	//Ne prend en param�tre que la fen�tre pour ensuite la donner � son AccueilHUD
	AccueilLogic(sf::RenderWindow& window);

	//Destructeur
	~AccueilLogic();

	//Permet d'indiquer � AccueilHUD qu'il faut mettre � jour l'affichage
	void update(bool booting);

	//Retourne l'indice du bouton cliqu�
	int buttonClicked(float x, float y);
};