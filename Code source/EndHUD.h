// Class EndHUD
// Auteur: Pierre TASSAN et Lo�c Chamberlin
// Objectif: G�rer l'affichage graphique du menu de Game Over
//

#pragma once
#include <SFML/Graphics.hpp>
#include <sfeMovie/Movie.hpp>
#include <sfeMovie/Visibility.hpp>
#include <sfeMovie/StreamSelection.hpp>
#include <iostream>
#include <SFML\Audio\Music.hpp>

#include "VideoLoader.h"
#include "TextureLoader.h"
#include "ComplexText.h"
#include "SoundLoader.h"

class EndHUD {
private:
	//R�f�rence � la fen�tre pour pouvoir g�rer l'affichage
	sf::RenderWindow* window_;

	//Vid�o et son d'arri�re plan
	VideoLoader* videoarriereplan_;
	SoundLoader* sonarriereplan_;

	//Statistiques sur la partie
	ComplexText* score_;
	ComplexText* keyboardstats_;
	ComplexText* speed_;

	//Texte du game over
	ComplexText* commentaire_;

	//Bouton
	ComplexText* quitter_;
	

public:
	//Constructeur 
	//Ne prend pour param�tre que la fen�tre
	EndHUD(sf::RenderWindow& window);

	//Destructeur
	~EndHUD();

	//Fonction appel�e � chaque actualisation, permet de mettre � jour l'affichage sur l'�cran du menu principal
	//Prend en param�tre les diff�rentes informations de la partie pour les afficher
	//On envoit les informations � chaque actualisation pour �tre compatible avec le menu de test d�veloppeur qui permet de modifier ces statistiques en direct
	void refresh(int gamemode = 0, int score = 0, int highestCombo = 0, int nbWord = 0, int nbBonCaractere = 0, int nbMauvaisCaractere = 0, int timeInSecond = 0);
	
	//Permet de retourner le pointeur des diff�rents textes affich�s � l'�cran, utilis� dans GameLogic pour d�terminer si des boutons ont �t� cliqu�s
	ComplexText* getQuitButton() { return quitter_; };
};